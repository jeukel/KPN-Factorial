#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Module.py
#
#  Copyright 2018 Daniel Jenkins <jeukel@Galago>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
import Queue
import threading
import time

def multi(q,d,t):
    while(True):
        name = threading.currentThread().getName()
        print "Thread: {0} start get item from queue[current size = {1}] at time" \
                " = {2} \n".format(name, q.qsize(), time.strftime('%H:%M:%S'))
        print "Thread: {0} start get item from queue[current size = {1}] at time" \
                " = {2} \n".format(name, d.qsize(), time.strftime('%H:%M:%S'))
        item = q.get();
        iteM = d.get();

        item = item * iteM

        print "Thread: {0} finish process item from queue[current size = {1}]" \
            " at time = {2} \n".format(name, q.qsize(), time.strftime('%H:%M:%S'))
        print "Thread: {0} finish process item from queue[current size = {1}]" \
            " at time = {2} \n".format(name, d.qsize(), time.strftime('%H:%M:%S'))
        q.task_done()
        d.task_done()

        print "Thread: {0} start put item into queue[current size = {1}] at" \
            " time = {2} \n".format(name, t.qsize(), time.strftime('%H:%M:%S'))
        item = 1
        t.put(item)
        print "Thread: {0} successfully put item into queue[current size = {1}]" \
            " at time = {2} \n".format(name, t.qsize(), time.strftime('%H:%M:%S'))

        t.join()


def addCicle(q,d):

    adder = 1
    while(True):

        name = threading.currentThread().getName()
        print "Thread: {0} start get item from queue[current size = {1}] at time" \
                " = {2} \n".format(name, q.qsize(), time.strftime('%H:%M:%S'))
        item = q.get()
        item += adder
        print "Thread: {0} finish process item from queue[current size = {1}] at" \
            " time = {2} \n".format(name, q.qsize(), time.strftime('%H:%M:%S'))
        q.task_done()

        print "Thread: {0} start put item into queue[current size = {1}] at time" \
                " = {2} \n".format(name, d.qsize(), time.strftime('%H:%M:%S'))
        d.put(item)
        print "Thread: {0} successfully put item into queue[current size = {1}]" \
            " at time = {2} \n".format(name, d.qsize(), time.strftime('%H:%M:%S'))
        d.join()


def delay(q,d):

    init_value = 1
    while(True):

        name = threading.currentThread().getName()
        if(not d.empty()):
            print "Thread: {0} start get item from queue[current size = {1}] at" \
                " time = {2} \n".format(name, q.qsize(), time.strftime('%H:%M:%S'))
            item = q.get();
            time.sleep(1)  # spend 3 seconds to process or consume the item
            print "Thread: {0} finish process item from queue[current size =" \
            " {1}] at time = {2} \n".format(name, q.qsize(), time.strftime('%H:%M:%S'))
            q.task_done()
            init_value = item

        else:
            print "Thread: {0} start put item into queue[current size = {1}] at" \
            " time = {2} \n".format(name, d.qsize(), time.strftime('%H:%M:%S'))

        d.put(init_value)
        print "Thread: {0} successfully put item into queue[current size = {1}]" \
            " at time = {2} \n".format(name, d.qsize(), time.strftime('%H:%M:%S'))
        d.join()


def split(q,d,t):

    while(True):

        name = threading.currentThread().getName()
        print "Thread: {0} start get item from queue[current size = {1}] at time" \
                "= {2} \n".format(name, q.qsize(), time.strftime('%H:%M:%S'))
        item = q.get();
        time.sleep(1)  # spend 3 seconds to process or consume the item
        print "Thread: {0} finish process item from queue[current size = {1}] " \
            "at time = {2} \n".format(name, q.qsize(), time.strftime('%H:%M:%S'))
        q.task_done()

        print "Thread: {0} start put item into queue[current size = {1}] at time" \
                " = {2} \n".format(name, d.qsize(), time.strftime('%H:%M:%S'))
        d.put(item)
        print "Thread: {0} successfully put item into queue[current size = {1}]" \
            " at time = {2} \n".format(name, d.qsize(), time.strftime('%H:%M:%S'))
        d.join()

        print "Thread: {0} start put item into queue[current size = {1}] at time" \
                " = {2} \n".format(name, t.qsize(), time.strftime('%H:%M:%S'))
        t.put(item)
        print "Thread: {0} successfully put item into queue[current size = {1}]" \
            " at time = {2} \n".format(name, t.qsize(), time.strftime('%H:%M:%S'))
        t.join()


def pseudosplit(q,d):

    while(True):
        name = threading.currentThread().getName()
        print "Thread: {0} start get item from queue[current size = {1}] at time" \
                "= {2} \n".format(name, q.qsize(), time.strftime('%H:%M:%S'))
        item = q.get();
        time.sleep(1)  # spend 3 seconds to process or consume the item
        print "Thread: {0} finish process item from queue[current size = {1}] " \
            "at time = {2} \n".format(name, q.qsize(), time.strftime('%H:%M:%S'))
        q.task_done()

        print "Thread: {0} start put item into queue[current size = {1}] at time" \
                " = {2} \n".format(name, d.qsize(), time.strftime('%H:%M:%S'))
        d.put(item)
        print "Thread: {0} successfully put item into queue[current size = {1}]" \
            " at time = {2} \n".format(name, d.qsize(), time.strftime('%H:%M:%S'))
        d.join()

        print "Factorial Result: " + str(item)


def main(args):
    size = 0

    a = Queue.Queue(maxsize=size)
    b = Queue.Queue(maxsize=size)
    c = Queue.Queue(maxsize=size)
    d = Queue.Queue(maxsize=size)
    e = Queue.Queue(maxsize=size)
    f = Queue.Queue(maxsize=size)
    g = Queue.Queue(maxsize=size)
    h = Queue.Queue(maxsize=size)

    multiVar = threading.Thread(name = "MultiplierThread", target=multi, args=(a,d,b,))
    multiVar.start()

    add = threading.Thread(name = "AdderThread", target=addCicle, args=(f,g,))
    add.start()

    del1 = threading.Thread(name = "DelayThread-"+str(01), target=delay, args=(b,c,))
    del1.start()

    del2 = threading.Thread(name = "DelayThread-"+str(02), target=delay, args=(g,e,))
    del2.start()

    split1 = threading.Thread(name = "SplitThread-"+str(01), target=pseudosplit, args=(c,a,))
    split1.start()

    split2 = threading.Thread(name = "Split02Thread-"+str(02), target=split, args=(e,f,d,))
    split2.start()


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
